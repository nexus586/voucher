package com.nexus.voucher;

import com.nexus.voucher.repositories.RoleRepository;
import com.nexus.voucher.repositories.UserRepository;
import com.nexus.voucher.shared.SeedDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.password.PasswordEncoder;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;


@SpringBootApplication
@EnableSwagger2
@PropertySource("classpath:application.properties")
public class VoucherApplication implements CommandLineRunner{

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


	public static void main(String[] args) {
		SpringApplication.run(VoucherApplication.class, args);
	}

    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("PayApp")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/.*"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Voucher")
                .description("An app to produce vouchers")
                .termsOfServiceUrl("http://cannotcontainallthisawesomeness.com")
                .contact("danagbemava@gmail.com")
                .license("Apache License Version 7.0")
                .licenseUrl("http://cannotcontainallthisawesomeness.com/LICENSE")
                .version("7.0")
                .build();

    }

    @Override
    public void run(String... args){
	    SeedDatabase sd =  new SeedDatabase(roleRepository, userRepository, passwordEncoder);

	    sd.createRoles();
	    sd.createAdmin();
    }


}
