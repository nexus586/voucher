package com.nexus.voucher.repositories;

import com.nexus.voucher.model.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;


public interface VoucherRepository extends JpaRepository<Voucher, Long> {

    Collection<Voucher> findAllByGeneratedBy_Id(Long id);
    Voucher findByCode(String code);
}
