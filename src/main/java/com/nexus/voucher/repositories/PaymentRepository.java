package com.nexus.voucher.repositories;

import com.nexus.voucher.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface PaymentRepository extends JpaRepository<Payment, Long> {

    Collection<Payment> findAllByClient_Id(Long clientId);
    Collection<Payment> findAllByMerchant_Id(Long merchantId);
}
