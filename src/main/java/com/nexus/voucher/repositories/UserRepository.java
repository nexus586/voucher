package com.nexus.voucher.repositories;

import com.nexus.voucher.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByEmail(String email);
    User findUserById(Long id);
    User findUserByAccountNumber(String accountNumber);
}
