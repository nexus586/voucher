package com.nexus.voucher.repositories;

import com.nexus.voucher.shared.Role;
import com.nexus.voucher.shared.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(RoleType roleName);
}
