package com.nexus.voucher.shared;

import com.nexus.voucher.model.User;
import com.nexus.voucher.repositories.RoleRepository;
import com.nexus.voucher.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class Shared {

    static final Logger logger = LoggerFactory.getLogger(Shared.class);

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    public Shared(UserRepository userRepository,
                  RoleRepository roleRepository,
                  PasswordEncoder passwordEncoder){
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    public Responses registerUser(User user, Collection<Role> userRole){
        Responses response = new Responses();

        if(userRepository.findUserByEmail(user.getEmail()) == null){
            User mUser = new User();
            mUser.setPhone(user.getPhone());
            mUser.setName(user.getName());
            mUser.setRoles(userRole);
            mUser.setPassword(passwordEncoder.encode(user.getPassword()));
            mUser.setEmail(user.getEmail());
            mUser.setEnabled(false);
            mUser.setAccountNumber(user.getAccountNumber());
            try{
                userRepository.save(mUser);
            }catch (Exception e){
                logger.error("An Unexpected error occurred", e);
            }
        }else {
            logger.error("User already exists");
            response.setStatus("Error");
            response.setReason("User already exists");
            response.setCode("500");
            return response;
        }
        response.setCode("200");
        response.setStatus("Success");
        response.setReason("Account Successfully created. " +
                "Please wait for the admin to activate your account");
        return response;
    }
}
