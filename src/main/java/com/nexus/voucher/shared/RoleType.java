package com.nexus.voucher.shared;

public enum RoleType {
    ROLE_MERCHANT, ROLE_ADMIN, ROLE_CLIENT
}
