package com.nexus.voucher.shared;

import com.nexus.voucher.model.Voucher;

public class VoucherResponse extends Responses{

    private Voucher voucher;

    public Voucher getVoucher() {
        return voucher;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }
}
