package com.nexus.voucher.shared;

import com.nexus.voucher.model.User;
import com.nexus.voucher.repositories.RoleRepository;
import com.nexus.voucher.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class SeedDatabase {

    static final Logger logger = LoggerFactory.getLogger(SeedDatabase.class);


    private RoleRepository roleRepository;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public SeedDatabase(RoleRepository roleRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public SeedDatabase() {
    }

    public void createRoles(){
        logger.info("Create roles and admin has been called");

        if(roleRepository.findAll().toArray().length == 0){
            logger.info("Role creation has begun");
            Role merchantRole = new Role();
            merchantRole.setName(RoleType.ROLE_MERCHANT);
            roleRepository.save(merchantRole);

            Role adminRole = new Role();
            adminRole.setName(RoleType.ROLE_ADMIN);
            roleRepository.save(adminRole);


            Role clientRole = new Role();
            clientRole.setName(RoleType.ROLE_CLIENT);
            roleRepository.save(clientRole);

        }
    }

    public void createAdmin(){
        Collection<Role> adminRoles = new ArrayList<>();
        adminRoles.add(roleRepository.findByName(RoleType.ROLE_ADMIN));

        try{
            logger.info("Attempting to create admin user");
            User user = userRepository.findUserByEmail(AppConstants.ADMIN_EMAIL);
            if(user == null){
                user = new User();
                user.setEnabled(true);
                user.setEmail(AppConstants.ADMIN_EMAIL);
                user.setPassword(passwordEncoder.encode(AppConstants.ADMIN_PASS));
                user.setName(AppConstants.ADMIN_NAME);
                user.setPhone(AppConstants.ADMIN_PHONE);
                user.setRoles(adminRoles);
                userRepository.save(user);
            }
        }catch (Exception e){
            logger.error("Error has occurred", e);
        }
    }
}
