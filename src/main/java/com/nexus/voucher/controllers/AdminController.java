package com.nexus.voucher.controllers;

import com.nexus.voucher.mail.MailManager;
import com.nexus.voucher.model.IUser;
import com.nexus.voucher.model.Payment;
import com.nexus.voucher.model.User;
import com.nexus.voucher.model.Voucher;
import com.nexus.voucher.repositories.PaymentRepository;
import com.nexus.voucher.repositories.RoleRepository;
import com.nexus.voucher.repositories.UserRepository;
import com.nexus.voucher.repositories.VoucherRepository;
import com.nexus.voucher.shared.AppConstants;
import com.nexus.voucher.shared.Responses;
import com.nexus.voucher.shared.Role;
import com.sun.org.apache.regexp.internal.RE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;

@RestController
@RequestMapping("/admin/")
public class AdminController {

    static final Logger logger = LoggerFactory.getLogger(AdminController.class);

    private UserRepository userRepository;
    private VoucherRepository voucherRepository;
    private PaymentRepository paymentRepository;
    private MailManager mailManager;
    private RoleRepository roleRepository;

    public AdminController(UserRepository userRepository,
                           VoucherRepository voucherRepository,
                           PaymentRepository paymentRepository,
                           MailManager mailManager,
                           RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.voucherRepository = voucherRepository;
        this.paymentRepository = paymentRepository;
        this.mailManager = mailManager;
        this.roleRepository = roleRepository;
    }

    @PostMapping("activateUser")
    public @ResponseBody ResponseEntity<Responses> activateUserAccount(@RequestBody IUser user){
        logger.info("Activate User has been called");
        Responses response = new Responses();

        User foundUser = userRepository.findUserById(user.getId());

//        response = checkIfUserExists(foundUser);
//        if(response.getCode().equalsIgnoreCase("404")){
//            return ResponseEntity.badRequest().body(response);
//        }

        try{
            if(foundUser.isEnabled()){
                response.setCode("304");
                response.setStatus("Error");
                response.setReason("User has already been activated");
                return ResponseEntity.badRequest().body(response);
            }

            foundUser.setEnabled(true);
            userRepository.save(foundUser);

            mailManager.setMessage("Your account has been activated. " +
                    "\nYou can now start using this service. " +
                    "\nThank you");
            mailManager.sendAccountNotification(foundUser);
            response.setStatus("success");
            response.setCode("200");
            response.setReason("Account has been successfully activated");
        }catch (Exception e){
            logger.error("An unexpected error occurred", e);
            response.setCode("500");
            response.setStatus("error");
            response.setReason("An unexpected error occurred");
            return ResponseEntity.badRequest().body(response);
        }

        return ResponseEntity.ok(response);
    }

    @PostMapping("deactivateUser")
    public @ResponseBody ResponseEntity<Responses> deactivateUserAccount(@RequestBody IUser user){
        logger.info("deactivate user has started");
        Responses response = new Responses();

        User foundUser = userRepository.findUserById(user.getId());

//        response = checkIfUserExists(foundUser);

//        if(response.getCode().equalsIgnoreCase("404")){
//            return ResponseEntity.badRequest().body(response);
//        }

        try{
            foundUser.setEnabled(false);
            userRepository.save(foundUser);
            response.setCode("200");
            response.setReason("User successfully deactivated");
            response.setStatus("Success");
        }catch (Exception e){
            logger.info("An error occurred");
            response.setCode("500");
            response.setReason("Internal Server Error");
            response.setStatus("Error");
            return ResponseEntity.badRequest().body(response);
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping("findAllUsers")
    public Collection<User> findAllUsers(){
        return userRepository.findAll();
    }

    @GetMapping("viewAllPayments")
    public Collection<Payment> findAllPayments(){
        return paymentRepository.findAll();
    }

    @GetMapping("findAllVouchers")
    public Collection<Voucher> findAllVouchers(){
        return voucherRepository.findAll();
    }

    @GetMapping("findAllRoles")
    public Collection<Role> findAllRoles(){
        return roleRepository.findAll();
    }

//    private Responses checkIfUserExists(User foundUser){
//        Responses response = new Responses();
//        if(userRepository.findUserById(foundUser.getId()) == null){
//            logger.error("User not found");
//            response.setCode("404");
//            response.setStatus("error");
//            response.setReason("User not found");
//        }
//        return response;
//    }
}
