package com.nexus.voucher.controllers;

import com.nexus.voucher.mail.MailManager;
import com.nexus.voucher.model.Payment;
import com.nexus.voucher.model.User;
import com.nexus.voucher.model.Voucher;
import com.nexus.voucher.repositories.PaymentRepository;
import com.nexus.voucher.repositories.RoleRepository;
import com.nexus.voucher.repositories.UserRepository;
import com.nexus.voucher.repositories.VoucherRepository;
import com.nexus.voucher.shared.Responses;
import com.nexus.voucher.shared.Role;
import com.nexus.voucher.shared.RoleType;
import com.nexus.voucher.shared.Shared;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequestMapping("/client/")
@RestController
public class ClientController {

    static final Logger logger = LoggerFactory.getLogger(ClientController.class);

    private UserRepository userRepository;
    private VoucherRepository voucherRepository;
    private MailManager mailManager;
    private PaymentRepository paymentRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    public ClientController(UserRepository userRepository,
                            VoucherRepository voucherRepository,
                            MailManager mailManager,
                            PaymentRepository paymentRepository,
                            RoleRepository roleRepository,
                            PasswordEncoder passwordEncoder){
        this.userRepository = userRepository;
        this.voucherRepository = voucherRepository;
        this.mailManager = mailManager;
        this.paymentRepository = paymentRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @PostMapping("main/generateVoucher")
    public @ResponseBody ResponseEntity<Responses> generateVoucher(@RequestParam(name = "id")Long id, @RequestBody Voucher voucher){
        User user = userRepository.findUserById(id);

        Responses generatedVoucherResponse = new Responses();

        if(user != null){
            Voucher mVoucher = new Voucher();
            String voucherCode = UUID.randomUUID().toString();
            mVoucher.setCode(voucherCode);
            mVoucher.setGeneratedBy(user);
            mVoucher.setAmount(voucher.getAmount());
            mVoucher.setDescription(voucher.getDescription());
            mVoucher.setAmount(voucher.getAmount());
            try{
                voucherRepository.save(mVoucher);
                mailManager.setMessage("You have successfully generated the voucher with code: [ "+voucherCode+" ]." +
                        "\nPlease do not give out the voucher to any other person." +
                        "\nYour account was debited by GHc"+voucher.getAmount()+
                        "\nThank You");
                mailManager.sendVoucher(user);

                generatedVoucherResponse.setCode("200");
                generatedVoucherResponse.setStatus("Success");
                generatedVoucherResponse.setReason("Voucher successfully generated");
                voucherRepository.save(mVoucher);
            }catch (Exception e){
                logger.error("[An error occurred]", e);
                generatedVoucherResponse.setCode("401");
                generatedVoucherResponse.setStatus("Error");
                generatedVoucherResponse.setReason("An unexpected error occurred");
                return ResponseEntity.badRequest().body(generatedVoucherResponse);
            }

        }else {
            generatedVoucherResponse.setCode("404");
            generatedVoucherResponse.setStatus("Not found");
            generatedVoucherResponse.setReason("User not found");
            return ResponseEntity.badRequest().body(generatedVoucherResponse);
        }

        return ResponseEntity.ok(generatedVoucherResponse);
    }

    @PostMapping("register")
    public @ResponseBody ResponseEntity<Responses> register(@RequestBody User user){
        logger.info("Register has been called");
        Responses response;

        logger.info("finding client role");
        Collection<Role> clientRole = new ArrayList<>();
        clientRole.add(roleRepository.findByName(RoleType.ROLE_CLIENT));

        response = new Shared(userRepository, roleRepository, passwordEncoder).registerUser(user, clientRole);

        return ResponseEntity.badRequest().body(response);
    }

    @GetMapping("main/findAllVouchers")
    public Collection<Voucher> findAllVouchers(@RequestParam(name = "id")Long id){
        User user = userRepository.findUserById(id);
        if(user != null){
            return voucherRepository.findAllByGeneratedBy_Id(id);
        }
        return Collections.emptyList();
    }

    @GetMapping("main/findAllPayments")
    public Collection<Payment> findAllPayments(@RequestParam(name = "id")Long id){
        User user = userRepository.findUserById(id);
        if(user != null){
            return this.paymentRepository.findAllByClient_Id(id);
        }

        return Collections.emptyList();
    }
}
