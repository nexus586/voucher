package com.nexus.voucher.controllers;

import com.nexus.voucher.config.details.JWTUser;
import com.nexus.voucher.model.User;
import com.nexus.voucher.config.jwt.JwtTokenHelper;
import com.nexus.voucher.model.Login;
import com.nexus.voucher.model.LoginResponse;
import com.nexus.voucher.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user/")
public class UserController {

    static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private JwtTokenHelper helper;

    private AuthenticationManager manager;
    private UserDetailsService userDetailsService;
    private UserRepository userRepository;

    public UserController(JwtTokenHelper helper,
                          AuthenticationManager manager,
                          UserDetailsService userDetailsService,
                          UserRepository userRepository) {
        this.helper = helper;
        this.manager = manager;
        this.userDetailsService = userDetailsService;
        this.userRepository = userRepository;
    }

    @PostMapping(value = "login", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody LoginResponse login(@RequestBody Login user){
        LoginResponse response = new LoginResponse();

        Authentication authentication = null;

        try {
            authentication = manager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword())
            );
        }catch (Exception ex){
            logger.error("Error: ", ex);
            response.setCode(101);
            response.setMessage(ex.getMessage());
            return response;
        }

        logger.info("[authentication] =>"+authentication);

        SecurityContextHolder.getContext().setAuthentication(authentication);




        final JWTUser userDetails = (JWTUser) userDetailsService.loadUserByUsername(user.getEmail());
        final String token = helper.generateToken(userDetails);

        try{
            User authUser = userRepository.findUserByEmail(userDetails.getEmail());
            authUser.setLoggedIn(true);
            userRepository.save(authUser);
        }catch (Exception e){
            logger.error("An error occurred: ", e);
            response.setCode(500);
            response.setMessage("Login failure");
            return response;
        }

        response.setCode(1000);
        response.setToken(token);
        response.setRole(userDetails.getAuthorities());
        response.setId(userDetails.getId());
        response.setName(userDetails.getName());
        response.setMessage("Login Successful");

        return response;
    }
}
