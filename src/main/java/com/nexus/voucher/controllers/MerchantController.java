package com.nexus.voucher.controllers;

import com.nexus.voucher.mail.MailManager;
import com.nexus.voucher.model.IVoucher;
import com.nexus.voucher.model.Payment;
import com.nexus.voucher.model.User;
import com.nexus.voucher.model.Voucher;
import com.nexus.voucher.repositories.PaymentRepository;
import com.nexus.voucher.repositories.RoleRepository;
import com.nexus.voucher.repositories.UserRepository;
import com.nexus.voucher.repositories.VoucherRepository;
import com.nexus.voucher.shared.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("/merchant/")
public class MerchantController {

    static final Logger logger = LoggerFactory.getLogger(MerchantController.class);

    private VoucherRepository voucherRepository;
    private PaymentRepository paymentRepository;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private RoleRepository roleRepository;
    private MailManager mailManager;

    public MerchantController(VoucherRepository voucherRepository,
                              PaymentRepository paymentRepository,
                              UserRepository userRepository,
                              PasswordEncoder passwordEncoder,
                              RoleRepository roleRepository,
                              MailManager mailManager){
        this.voucherRepository = voucherRepository;
        this.passwordEncoder = passwordEncoder;
        this.paymentRepository = paymentRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.mailManager = mailManager;
    }

    @PostMapping("main/verify")
    public @ResponseBody ResponseEntity<VoucherResponse> verifyVoucher(@RequestBody IVoucher voucherCode){
        VoucherResponse responses = new VoucherResponse();
        logger.info("Verify voucher has been called");
        try{
            if(voucherRepository.findByCode(voucherCode.getVoucherCode()) == null){
                responses.setCode("404");
                responses.setStatus("Error");
                responses.setReason("Voucher not found");
                logger.error("An error occurred");
                return ResponseEntity.badRequest().body(responses);
            }
        }catch (Exception e){
            logger.error("An error occurred",e);
        }

        responses.setVoucher(voucherRepository.findByCode(voucherCode.getVoucherCode()));
        responses.setReason("Voucher verified");
        responses.setStatus("Success");
        responses.setCode("200");
        return ResponseEntity.ok(responses);
    }

    @PostMapping("register")
    public @ResponseBody ResponseEntity<Responses> register(@RequestBody User user){
        logger.info("Register has been called");
        Responses response;

        logger.info("finding merchant role");
        Collection<Role> merchantRole = new ArrayList<>();
        merchantRole.add(roleRepository.findByName(RoleType.ROLE_MERCHANT));

        response = new Shared(userRepository, roleRepository, passwordEncoder).registerUser(user, merchantRole);

        return ResponseEntity.ok(response);
    }

    @PostMapping("main/initiatePayment")
    public @ResponseBody ResponseEntity<Responses> initiatePayment(@RequestParam(name = "id")Long id, @RequestBody Payment payment){
        User merchant = userRepository.findUserById(id);
        Responses responses = new Responses();
        if(merchant == null){
            responses.setCode("404");
            responses.setStatus("Error");
            responses.setReason("User not found");
            return ResponseEntity.badRequest().body(responses);
        }

        Payment mPayment = new Payment();

        Voucher voucher = voucherRepository.findByCode(payment.getVoucherCode());
        User user = userRepository.findUserByAccountNumber(payment.getClientAccountNumber());

        if(voucher.isUsed()){
          responses.setStatus("Used");
          responses.setReason("Voucher has already been used");
          responses.setCode("400");
          return ResponseEntity.badRequest().body(responses);
        }

        try{
            voucher.setUsed(true);
            voucherRepository.save(voucher);
            mPayment.setMerchant(merchant);
            mPayment.setClient(user);
            mPayment.setVoucher(voucher);
            mPayment.setItemBought(payment.getItemBought());
            mPayment.setPrice(payment.getPrice());
            mPayment.setQuantity(payment.getQuantity());
            mPayment.setAmount(payment.getPrice() * payment.getQuantity());
            paymentRepository.save(mPayment);

            mailManager.sendVoucherUsageNotice(user);
            mailManager.setMessage("Your voucher with the code [ "+voucher.getCode()+" ] has been used to make a " +
                    "payment with the merchant " +
                    merchant.getName());
        }catch (Exception e){
            logger.error("An error occurred", e);
            responses.setCode("400");
            responses.setStatus("Error");
            responses.setReason("An error occurred. Please try again");
            return ResponseEntity.badRequest().body(responses);
        }

        responses.setStatus("success");
        responses.setReason("Payment successfully made");
        responses.setCode("200");
        return ResponseEntity.ok(responses);
    }

    @GetMapping("main/findAllPayments")
    public Collection<Payment> findAllPayments(@RequestParam(name = "id")Long id){
        return paymentRepository.findAllByMerchant_Id(id);
    }
}
