package com.nexus.voucher.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import javax.transaction.TransactionScoped;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "payments")
@JsonSerialize
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @OneToOne
    private User client;

    @NotNull
    @OneToOne/**(fetch=FetchType.EAGER)**/
    private User merchant;

    @NotNull
    @OneToOne
    private Voucher voucher;

    @Transient
    private String voucherCode;

    @Transient
    private String clientAccountNumber;

    @NotNull
    private String itemBought;

    @NotNull
    private int quantity;

    @NotNull
    private double price;

    @NotNull
    private double amount;

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public User getMerchant() {
        return merchant;
    }

    public void setMerchant(User merchant) {
        this.merchant = merchant;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }

    public String getItemBought() {
        return itemBought;
    }

    public void setItemBought(String itemBought) {
        this.itemBought = itemBought;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getClientAccountNumber() {
        return clientAccountNumber;
    }

    public void setClientAccountNumber(String clientAccountNumber) {
        this.clientAccountNumber = clientAccountNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
