package com.nexus.voucher.config.jwt;

import com.nexus.voucher.config.details.JWTUser;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends GenericFilterBean {

    static final Logger log = LoggerFactory.getLogger(JwtTokenFilter.class);

    private UserDetailsService userDetailsService;

    private JwtTokenHelper helper;

    public JwtTokenFilter(UserDetailsService userDetailsService, JwtTokenHelper helper) {
        this.helper = helper;
        this.userDetailsService = userDetailsService;
    }

    @Value("${jwt.token.header}")
    private String tokenHeader;

    @Value("${jwt.token.secret}")
    private String secretKey;

//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
//        log.debug("processing authentication for '{}'", request.getRequestURL());
//
//        final String requestHeader = request.getHeader(this.tokenHeader);
//
//        String username = null;
//        String authToken = null;
//        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
//            authToken = requestHeader.substring(7);
//            try {
//                username = helper.getUsernameFromToken(authToken);
//            } catch (IllegalArgumentException e) {
//                log.error("an error occurred during getting username from token", e);
//            } catch (ExpiredJwtException e) {
//                log.warn("the token is expired and not valid anymore", e);
//            }
//        } else {
//            log.warn("couldn't find bearer string, will ignore the header");
//        }
//
//        log.debug("checking authentication for user '{}'", username);
//        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
//            log.debug("security context was null, so authorizing user");
//
//            // It is not compelling necessary to load the use details from the database. You could also store the information
//            // in the token and read it from it. It's up to you ;)
//            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
//
//            // For simple validation it is completely sufficient to just check the token integrity. You don't have to call
//            // the database compellingly. Again it's up to you ;)
//            if (helper.validateToken(authToken, userDetails)) {
//                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
//                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//                log.info("authorizated user '{}', setting security context", username);
//                SecurityContextHolder.getContext().setAuthentication(authentication);
//            }
//        }
//
//        chain.doFilter(request, response);
//    }




    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {

        log.info("Secret Key '{}", secretKey);

        HttpServletRequest request = (HttpServletRequest) req;
        String authHeader = request.getHeader("Authorization");
        if (authHeader == null ) {
            throw new ServletException("Missing or invalid Authorization header.");
        }

        String token = authHeader;
        log.info("token '{}", authHeader);
        try {

            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
//            request.setAttribute("claims", claims);
        }
        catch (final SignatureException e) {
            throw new ServletException("Invalid token.");
        }

        filterChain.doFilter(req, res);
    }
}
