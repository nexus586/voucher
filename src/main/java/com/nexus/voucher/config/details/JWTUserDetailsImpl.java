package com.nexus.voucher.config.details;

import com.nexus.voucher.model.User;
import com.nexus.voucher.repositories.UserRepository;
import com.nexus.voucher.shared.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class JWTUserDetailsImpl implements UserDetailsService {

    private UserRepository userRepository;

    public JWTUserDetailsImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findUserByEmail(email);

        if(user == null){
            throw new UsernameNotFoundException("No user found with email: "+email);
        }else{
            return new JWTUser(
                    user.getId(),
                    user.getEmail(),
                    user.getName(),
                    user.getPassword(),
                    mapToGrantedAuthorities(user.getRoles()),
                    user.isEnabled()
            );
        }
    }

    private Collection<GrantedAuthority> mapToGrantedAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());
    }
}
