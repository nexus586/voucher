package com.nexus.voucher.config;

import com.nexus.voucher.config.jwt.JwtAuthenticationEntryPoint;
import com.nexus.voucher.config.jwt.JwtTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    JwtAuthenticationEntryPoint auet;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception{
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .exceptionHandling().authenticationEntryPoint(auet)
                .and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
//                .antMatchers(HttpMethod.POST, "/admin/").hasRole("ADMIN")
//                .antMatchers(HttpMethod.GET, "/admin/").hasRole("ADMIN")
//                .antMatchers(HttpMethod.POST, "/merchant/register").permitAll()
//                .antMatchers(HttpMethod.POST, "/merchant/main/**").hasRole("MERCHANT")
//                .antMatchers(HttpMethod.GET, "/merchant/main/**").hasRole("MERCHANT")
//                .antMatchers(HttpMethod.POST, "/client/register").permitAll()
//                .antMatchers(HttpMethod.POST, "/client/main/**").hasRole("CLIENT")
//                .antMatchers(HttpMethod.GET, "client/main/**").hasRole("CLIENT")
//                .antMatchers(HttpMethod.OPTIONS, "/user/**").permitAll()
//                .antMatchers(HttpMethod.GET, "/merchant/main/**").hasAuthority("ROLE_MERCHANT")
//                .antMatchers(HttpMethod.POST, "/merchant/main/**").hasAuthority("ROLE_MERCHANT")
//                .antMatchers(HttpMethod.OPTIONS, "/client/main/**").hasAuthority("ROLE_CLIENT")
//                .antMatchers(HttpMethod.OPTIONS, "/merchant/register").permitAll()
//                .antMatchers(HttpMethod.OPTIONS, "/client/register").permitAll()
//                .antMatchers(HttpMethod.OPTIONS, "/admin/**").hasAuthority("ROLE_ADMIN")
//                .antMatchers(HttpMethod.OPTIONS, "/admin/**").permitAll()
                .anyRequest().anonymous();


    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**"
        );
    }
}
