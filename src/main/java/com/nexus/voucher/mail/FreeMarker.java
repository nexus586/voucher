package com.nexus.voucher.mail;

import freemarker.template.Configuration;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.util.Map;

@Component
public class FreeMarker {

    private Configuration freeMarkerTemplateConfiguration;

    public FreeMarker(Configuration freeMarkerTemplateConfiguration){
        this.freeMarkerTemplateConfiguration = freeMarkerTemplateConfiguration;
    }

    @Bean
    public FreeMarkerConfigurationFactoryBean getFreeMarkerConfiguration(){
        FreeMarkerConfigurationFactoryBean fmConfigFactoryBean = new FreeMarkerConfigurationFactoryBean();
        fmConfigFactoryBean.setTemplateLoaderPath("/templates/");
        return fmConfigFactoryBean;
    }

    public String processDataModel(Map<String, Object> model, String fileName){
        //thread safe
        StringBuffer content = new StringBuffer();
        try{
            //append the processed html
            content.append(FreeMarkerTemplateUtils.processTemplateIntoString(
                    freeMarkerTemplateConfiguration.getTemplate(fileName),model));
            //return on success
            return content.toString();
        }catch(Exception e){
            System.out.println("Exception occurred while processing the template: "+e.getMessage());
        }
        //return nothing on success
        return "";
    }
}
