package com.nexus.voucher.mail;

import com.nexus.voucher.model.User;

public interface NotificationManager {

    void sendNotification(User user);
    void sendAccountNotification(User user);
    void sendAccountCreationNotification(User user);
    void sendVoucher(User user);
    void sendVoucherUsageNotice(User user);
}
