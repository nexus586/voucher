package com.nexus.voucher.mail;

import com.nexus.voucher.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Service
public class MailManager implements NotificationManager{

    static final Logger logger = LoggerFactory.getLogger(MailManager.class);

    @Value("${spring.mail.username}")
    private String senderEmail;

    @Value("${spring.mail.password}")
    private String senderPassword;

    @Autowired
    private JavaMailSender mailSender;

//    public MailManager(JavaMailSender mailSender, FreeMarker freeMarker) {
//        this.mailSender = mailSender;
//        this.freeMarker = freeMarker;
//    }
    @Autowired
    private FreeMarker freeMarker;

    private String message;

    public MailSender getMailSender(){
        return mailSender;
    }

    public void setMailSender(JavaMailSender mailSender){
        this.mailSender = mailSender;
    }

    private String getMessage(){
        return this.message;
    }

    public void setMessage(String message){
        this.message = message;
    }

    @Bean
    public JavaMailSender getJavaMailSender(){
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setPort(587);

        javaMailSender.setUsername(senderEmail);
        javaMailSender.setPassword(senderPassword);

        Properties props = javaMailSender.getJavaMailProperties();
        props.put("mail.transport.protocol","smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.debug", "true");

        return javaMailSender;
    }

    @Override
    public void sendAccountCreationNotification(User p) throws MailException {
        try{
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(p.getEmail());
            helper.setFrom("no-reply@payapp.com");
            helper.setSubject("Email Confirmation");
            Map<String, Object> dataModel = new HashMap<>();
            dataModel.put("user", p);
            dataModel.put("message", this.getMessage());
            helper.setText(freeMarker.processDataModel(dataModel, ""), true);
            this.mailSender.send(message);
        }catch (MessagingException me){
            logger.error("message not sent: ", me);
        }
    }

    @Override
    public void sendVoucher(User user) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(user.getEmail());
            helper.setFrom("no-reply@voucher.fasyl.com");
            helper.setSubject("Voucher Generation");
            Map<String, Object> datamodel = new HashMap<>();
            datamodel.put("user", user);
            datamodel.put("mimeMessage", this.getMessage());
            helper.setText(freeMarker.processDataModel(datamodel, "voucher.ftl"), true);
            this.mailSender.send(mimeMessage);
        }catch (Exception e){
            logger.error("[An unexpected error occurred]", e);
        }
    }

    @Override
    public void sendNotification(User p){

    }

    @Override
    public void sendAccountNotification(User p){
        try{
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(p.getEmail());
            helper.setFrom("no-reply@payapp.com");
            helper.setSubject("Account Creation");
            Map<String, Object> dataModel = new HashMap<>();
            dataModel.put("user", p);
            dataModel.put("mimeMessage", this.getMessage());
            helper.setText(freeMarker.processDataModel(dataModel, "activation.ftl"), true);
            this.mailSender.send(mimeMessage);
        }catch (Exception me){
           logger.error("message not sent: ", me);
        }
    }

    @Override
    public void sendVoucherUsageNotice(User user){
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(user.getEmail());
            helper.setFrom("no-reply@e-voucher.com");
            helper.setSubject("Voucher Usage");
            Map<String, Object> dataModel = new HashMap<>();
            dataModel.put("user", user);
            dataModel.put("mimeMessage", this.getMessage());
            helper.setText(freeMarker.processDataModel(dataModel, "usage.ftl"), true);
            this.mailSender.send(mimeMessage);
        }catch (Exception me){
            logger.error("[Message not sent] '{}'", me);
        }
    }
}
