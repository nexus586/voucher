<!Doctype html>
<html>
<head>
    <title>Account Activated</title>
    <link rel="stylesheet" href="../styles/bootstrap.min.css">
    <link rel="stylesheet" href="../styles/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="../styles/bootstrap-grid.min.css">
    <script type="application/javascript" src="../js/bootstrap.bundle.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body class="container">
    <div class="row">
        <h1>Dear ${user.name}</h1>
        <div class="col-md-6">
            <p>
                ${mimeMessage}
            </p>
        </div>
    </div>
</body>
</html>